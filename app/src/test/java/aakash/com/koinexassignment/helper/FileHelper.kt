package aakash.com.koinexassignment.helper

import android.support.annotation.VisibleForTesting
import java.io.BufferedReader
import java.io.FileReader
import java.io.IOException

/**
 * Created by stllpt031 on 15/10/18.
 */
object FileHelper {
    private const val baseAssetsPath = "src/test/java/aakash/com/koinexassignment/asset"
    @VisibleForTesting
    private fun loadStringFromFile(fileName: String): String? {
        return try {
            val reader = BufferedReader(FileReader(fileName))
            val stringBuilder = StringBuilder()
            val ls = System.getProperty("line.separator")
            for (line in reader.lines()) {
                stringBuilder.append(line)
                stringBuilder.append(ls)
            }
            stringBuilder.deleteCharAt(stringBuilder.length - 1)
            reader.close()

            stringBuilder.toString()
        } catch (e: IOException) {
            null
        }
    }

    @VisibleForTesting
    fun loadCurrencyItems(): String? {
        return loadStringFromFile("$baseAssetsPath/private_medication_search_response.json")
    }

}