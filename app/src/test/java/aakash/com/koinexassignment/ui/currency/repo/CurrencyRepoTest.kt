package aakash.com.koinexassignment.ui.currency.repo

import aakash.com.koinexassignment.helper.FileHelper
import aakash.com.koinexassignment.helper.whenever
import aakash.com.koinexassignment.shareddata.endpoints.ApiEndPoints
import aakash.com.koinexassignment.ui.currency.model.CurrencyResponseModel
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.*
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response

@RunWith(MockitoJUnitRunner::class)
class CurrencyRepoTest {

    lateinit var currencyRepo: CurrencyRepo

    @Mock
    lateinit var mApi: ApiEndPoints

    lateinit var mObservable: Observable<Response<CurrencyResponseModel>>
    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler {
            Schedulers.trampoline()
        }
    }

    @Test
    fun getCurrencyData() {
        val data = FileHelper.loadCurrencyItems()
        val response = Gson().fromJson<CurrencyResponseModel>(data, CurrencyResponseModel::class.java)

        mObservable = Observable.just(Response.success(response))

        whenever(mApi.fetchCurrencyList()).thenReturn(mObservable)
        currencyRepo = CurrencyRepo(mApi)
        currencyRepo.getCurrencyData(true)
        currencyRepo.data.value?.data?.let {
            Assert.assertEquals(it.data.size, 0)
        }
    }
}