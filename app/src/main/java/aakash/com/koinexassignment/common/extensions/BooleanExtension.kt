package aakash.com.koinexassignment.common.extensions

import android.view.View

/**
 * Created by aakashmehta on 16/2/18.
 */
fun Boolean.isVisible() = if (this) View.VISIBLE else View.GONE