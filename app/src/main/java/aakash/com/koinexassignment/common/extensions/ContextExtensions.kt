package aakash.com.koinexassignment.common.extensions

import aakash.com.koinexassignment.AppApplication
import aakash.com.koinexassignment.BuildConfig
import aakash.com.koinexassignment.R
import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.support.annotation.ColorRes
import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment

/**
 * Created by stllpt065 on 6/7/17.
 */
/**
 * Toast to display String as Charsequence
 */
fun Context.charToast(char: CharSequence) = Toast.makeText(this, char, Toast.LENGTH_LONG).show()

/**
 * Toast to display String from Resource file as int
 */
fun Context.resToast(res: Int) = Toast.makeText(this, res, Toast.LENGTH_SHORT).show()

fun Context.longCharToast(char: CharSequence) = Toast.makeText(this, char, Toast.LENGTH_LONG).show()

fun Activity.resToast(@StringRes res: Int) {
    val li = this.layoutInflater
    val toast = Toast(this)
    toast.apply {
        duration = Toast.LENGTH_SHORT
        setText(res)
        show()
    }
}

fun Activity.resToast(message: String) {
    val li = this.layoutInflater
    val toast = Toast(this)
    toast.apply {
        duration = Toast.LENGTH_SHORT
        setText(message)
        show()
    }
}

fun log(msg: Any?, tag: String? = null) {
    val tagName = tag?.let {
        it
    } ?: AppApplication.getAppContext().applicationContext.applicationInfo.name + "Logs"

    if (BuildConfig.DEBUG) {
        Log.d(tagName, "$msg")
    }
}

fun Throwable.printDebugStackTrace() {
    if (BuildConfig.DEBUG) {
        this.printStackTrace()
    }
}

fun Context?.isInternetAvailable(): Boolean {
    return try {
        val connectivityManager = this?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivityManager.activeNetworkInfo != null &&
                (connectivityManager.activeNetworkInfo.isConnected)
    } catch (ex: Exception) {
        ex.printDebugStackTrace()
        false
    }
}

fun Context.getCompatDrawable(@DrawableRes id: Int): Drawable? =
        ContextCompat.getDrawable(this, id)

fun Context.getCompatColor(@ColorRes id: Int): Int =
        ContextCompat.getColor(this, id)

fun Activity.requestRequiredPermission(permissions: Array<String>, requestCode: Int) {
    ActivityCompat.requestPermissions(this, permissions, requestCode)
}

fun Fragment.requestPermissionFromFragment(permissions: Array<String>, requestCode: Int) {
    this.requestPermissions(permissions, requestCode)
}

fun Context.checkRequiredPermission(permission: String): Boolean {
    return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED
}

fun Context.copyToClipBoard(textToCopy: String) {
    val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    val clip = ClipData.newPlainText(getString(R.string.app_name), textToCopy)
    clipboard.primaryClip = clip
}