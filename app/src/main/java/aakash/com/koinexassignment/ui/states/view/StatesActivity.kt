package aakash.com.koinexassignment.ui.states.view

import aakash.com.koinexassignment.R
import aakash.com.koinexassignment.ui.currency.model.CurrencyStates
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_states.*

class StatesActivity : AppCompatActivity() {

    private lateinit var currencyStates: CurrencyStates

    companion object {
        const val PARAM_SYMBOL_STATE = "SymbolState"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_states)
        setupValue()
    }

    private fun setupValue() {
        currencyStates = intent.getSerializableExtra(PARAM_SYMBOL_STATE) as CurrencyStates
        tvSymbolFullName.text = currencyStates.currencyFullForm
        tvSymbolShortName.text = String.format(getString(R.string.label_short_form),
            currencyStates.currencyShortForm)

        tvTradeVolume.text = currencyStates.tradeVolume
        tvLastTradedPrice.text = currencyStates.lastTradedPrice
        tvTradeMin24Hours.text = currencyStates.minHrs
        tvTradeLowestAsk.text = currencyStates.lowestAsk
        tvTradeMax24Hours.text = currencyStates.maxHrs
        tvPerChange.text = currencyStates.perChange
        tvHighestBid.text = currencyStates.highestBid
        tvBaseCurreny.text = currencyStates.baseCurrency
        tvLabel24Hours.text = currencyStates.volHrs
    }
}
