package aakash.com.koinexassignment.ui.currency.model

import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class CurrencyResponseModel(
    @SerializedName("stats")
    val stats: CurrencyDetails?,
    @SerializedName("prices")
    val price: CurrencyDetails?
)

data class CurrencyDetails(
    @SerializedName("true_usd")
    val trueUsd: JsonObject? = null,
    @SerializedName("inr")
    val inr: JsonObject? = null
)

data class CurrencyStates(
    @SerializedName("trade_volume")
    val tradeVolume: String? = "",
    @SerializedName("currency_full_form")
    val currencyFullForm: String? = "",
    @SerializedName("last_traded_price")
    val lastTradedPrice: String? = "",
    @SerializedName("min_24hrs")
    val minHrs: String? = "",
    @SerializedName("lowest_ask")
    val lowestAsk: String? = "",
    @SerializedName("max_24hrs")
    val maxHrs: String? = "",
    @SerializedName("currency_short_form")
    val currencyShortForm: String? = "",
    @SerializedName("per_change")
    val perChange: String? = "",
    @SerializedName("highest_bid")
    val highestBid: String? = "",
    @SerializedName("baseCurrency")
    val baseCurrency: String? = "",
    @SerializedName("vol_24hrs")
    val volHrs: String? = ""
) : Serializable

data class CurrencyModel(val symbol: String, val price : String, val state : CurrencyStates)

