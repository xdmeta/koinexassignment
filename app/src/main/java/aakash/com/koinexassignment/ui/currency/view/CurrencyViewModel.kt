package aakash.com.koinexassignment.ui.currency.view

import android.util.Log
import aakash.com.koinexassignment.ui.currency.model.CurrencyEventState
import aakash.com.koinexassignment.ui.currency.repo.CurrencyRepo
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject

/**
 * Created by aakashmehta on 28/8/18.
 */
class CurrencyViewModel  @Inject constructor(private val mCurrencyRepo: CurrencyRepo) : ViewModel() {
    private var userModel = MutableLiveData<CurrencyEventState>()
    fun getUserModel() = userModel

    fun getData(internetConnectivity: Boolean, refresh: Boolean = false) {
        if (userModel.value != null && !refresh) {
            return
        } else {
            userModel = mCurrencyRepo.getCurrencyData(internetConnectivity)
        }
    }

    override fun onCleared() {
        super.onCleared()
        Log.d("DISPOSE", "----- disposed -------")
        mCurrencyRepo.mCompositeDisposable.clear()
        mCurrencyRepo.mCompositeDisposable.dispose()
    }

}