package aakash.com.koinexassignment.ui.currency.view

import aakash.com.koinexassignment.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxbinding3.view.clicks
import kotlinx.android.synthetic.main.layout_currency_item.view.*

class CurrencyAdapter(
    private val itemList: ArrayList<Pair<String, String>>,
    private val onItemClick: (position: Int) -> Unit
) :
    RecyclerView.Adapter<CurrencyAdapter.CurrencyHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CurrencyHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_currency_item, parent, false)
        )

    override fun getItemCount() = itemList.size

    override fun onBindViewHolder(holder: CurrencyHolder, position: Int) {
        holder.itemView.apply {
            tvSymbol.text = itemList[position].first
            tvPrice.text = itemList[position].second
            clicks().subscribe {
                onItemClick(position)
            }
        }
    }

    inner class CurrencyHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}