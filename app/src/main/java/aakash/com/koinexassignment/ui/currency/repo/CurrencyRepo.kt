package aakash.com.koinexassignment.ui.currency.repo

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import aakash.com.koinexassignment.shareddata.endpoints.ApiEndPoints
import aakash.com.koinexassignment.ui.currency.model.*
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonObject
import java.lang.Exception
import javax.inject.Inject

/**
 * Created by aakashmehta on 28/8/18.
 */
open class CurrencyRepo @Inject constructor(@VisibleForTesting val mApi: ApiEndPoints) {
    val mCompositeDisposable = CompositeDisposable()

    @VisibleForTesting
    lateinit var data: MutableLiveData<CurrencyEventState>

    fun getCurrencyData(internetConnectivity: Boolean): MutableLiveData<CurrencyEventState> {
        data = MutableLiveData()
        if (!internetConnectivity) {
            data.value = CurrencyEventState(
                error = Error("Internet is not connected", true)
            )
        } else {
            mApi.fetchCurrencyList()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { data.value = CurrencyEventState(progress = true) }
                .subscribeBy(
                    onNext = { it ->
                        val currencyModels = ArrayList<CurrencyModel>()
                        try {
                            it.body()?.let { it1 ->
                                it1.price?.inr?.let { inrJson ->
                                    val currencySet = inrJson.keySet()
                                    currencySet?.let {
                                        val iterator = it.iterator()
                                        while (iterator.hasNext()) {
                                            val key = iterator.next()
                                            inrJson.get(key)
                                            val stateJson = it1.stats?.inr?.get(key) as JsonObject
                                            val currencyStates = Gson().fromJson<CurrencyStates>(
                                                stateJson,
                                                CurrencyStates::class.java
                                            )
                                            currencyModels.add(
                                                CurrencyModel(
                                                    key,
                                                    inrJson.get(key).asString,
                                                    currencyStates
                                                )
                                            )
                                        }
                                    }
                                }
                            }

                            data.value = CurrencyEventState(
                                progress = false,
                                data = ResponseData(currencyModels)
                            )
                        } catch (e: Exception) {

                            data.value = CurrencyEventState(
                                progress = false,
                                error = Error(e.message ?: "Error", true)
                            )
                            e.printStackTrace()
                        }

                    },
                    onError = {
                        data.value = CurrencyEventState(
                            progress = false,
                            error = it.message?.let { it1 -> Error(msg = it1, show = true) }
                        )
                    }
                ).addTo(mCompositeDisposable)
        }

        return data
    }
}