package aakash.com.koinexassignment.ui.currency.model

/**
 * Created by aakashmehta on 28/8/18.
 */
data class Error(val msg: String, val show: Boolean)

data class ResponseData(val data: ArrayList<CurrencyModel>)

data class CurrencyEventState(
        val error: Error? = Error("", show = false),
        val progress: Boolean = false,
        val data: ResponseData? = null
)

