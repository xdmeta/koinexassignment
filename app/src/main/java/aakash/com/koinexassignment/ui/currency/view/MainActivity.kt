package aakash.com.koinexassignment.ui.currency.view

import aakash.com.koinexassignment.AppApplication
import aakash.com.koinexassignment.R
import aakash.com.koinexassignment.common.extensions.*
import aakash.com.koinexassignment.shareddata.base.BaseActivity
import aakash.com.koinexassignment.ui.currency.model.CurrencyModel
import aakash.com.koinexassignment.ui.states.view.StatesActivity
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MainActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val currencyModels = ArrayList<CurrencyModel>()
    private val currencySet = ArrayList<Pair<String, String>>()

    private val mViewModel: CurrencyViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)[CurrencyViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as AppApplication).mComponent.inject(this)
        setContentView(R.layout.activity_main)
        setupUi()
        setupTimer()
        setupViewModel()
    }

    private fun setupTimer() {
        Observable.interval(1, TimeUnit.MINUTES)
            .subscribe {
                runOnUiThread { setupViewModel(true) }
            }.addTo(compositeDisposable)
    }

    private fun setupUi() {
        rvContent.layoutManager = LinearLayoutManager(this)
        rvContent.adapter = CurrencyAdapter(currencySet) {
            val intent = Intent(this, StatesActivity::class.java)
            intent.putExtra(StatesActivity.PARAM_SYMBOL_STATE, currencyModels[it].state)
            startActivity(intent)
        }
        srlParent.setOnRefreshListener {
            runOnUiThread { setupViewModel(true) }
        }
    }

    private fun setupViewModel(refresh: Boolean = false) {
        mViewModel.getData(isInternetAvailable(), refresh)
        mViewModel.getUserModel().observe(this, Observer { it ->
            it.error?.let {
                srlParent.isRefreshing = false
                tvError.visibility = it.show.isVisible()
                tvError.text = it.msg
            }
            it.data?.let { responseData ->
                srlParent.isRefreshing = false
                rvContent.visible()
                responseData.data.forEach {
                    currencySet.add(Pair(it.symbol, ": ${it.price}"))
                    currencyModels.add(it)
                }
                rvContent.adapter?.notifyDataSetChanged()
            } ?: let {
                rvContent.gone()
            }
            pbContent.visibility = it.progress.isVisible()
        })
    }
}