package aakash.com.koinexassignment

import aakash.com.koinexassignment.injection.component.AppComponent
import aakash.com.koinexassignment.injection.component.DaggerAppComponent
import aakash.com.koinexassignment.injection.module.RetrofitModule
import android.app.Application

/**
 * Created by aakashmehta on 10/2/19.
 */
class AppApplication : Application() {
    lateinit var mComponent: AppComponent

    companion object {

        private lateinit var instance: AppApplication

        fun getAppContext(): AppApplication {
            return instance
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        initDagger()
    }

    private fun initDagger() {
        mComponent = DaggerAppComponent.builder()
                .retrofitModule(RetrofitModule())
                .build()
    }

}