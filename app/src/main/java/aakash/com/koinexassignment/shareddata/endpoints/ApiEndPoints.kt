package aakash.com.koinexassignment.shareddata.endpoints

import aakash.com.koinexassignment.ui.currency.model.CurrencyResponseModel
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET

/**
 * Created by aakashmehta on 10/2/19.
 */
interface ApiEndPoints {
    @GET("api/ticker")
    fun fetchCurrencyList(): Observable<Response<CurrencyResponseModel>>
}