package aakash.com.koinexassignment.shareddata.models

import com.google.gson.JsonElement

/**
 * Created by aakashmehta on 10/2/19.
 */
data class GlobalResponse(val message: String,
                          val data: JsonElement)