package aakash.com.koinexassignment.injection.component

import dagger.Component
import aakash.com.koinexassignment.injection.module.RetrofitModule
import aakash.com.koinexassignment.injection.module.ViewModelModule
import aakash.com.koinexassignment.ui.currency.view.MainActivity
import javax.inject.Singleton

/**
 * Created by aakashmehta on 10/2/19.
 */
@Singleton
@Component(modules = [
    RetrofitModule::class,
    ViewModelModule::class
])
interface AppComponent {
    fun inject(activity: MainActivity)
}